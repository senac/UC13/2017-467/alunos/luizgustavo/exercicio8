/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/* Você é dono de uma livraria onde você vende livros para todos os estados. Cliente
uma classe venda que deve possuir um cliente , valor total de produtos e estado de
entrega.Com sua entrega pode ir para outro estado os valores de impostos como o
ICMS podem variar conforme o destino da mercadoria. ICMS -> Preço da mercadoria x
Alíquota = Valor do ICMS da mercadoria, ou seja, R$ 1.000,00 x 18% = R$ 180,00.
Nesse caso, o valor pago de ICMS será de R$ 180,00. Utilizando as seguintes alíquotas
:Venda para o RJ (17%) Venda Para SP(18%)e Venda para MA (12%) crie uma classe
calculadora de ICMS que deve possuir um método calcular que após o calculo deve
emitir a guia para o recolhimento do imposto.

 */
public class Vendas {
    public static final double RJ = 0.17;
    public static final double SP = 0.18;
    public static final double MA = 0.12;
    
    private String cliente;
    private double valorProduto;
    private String estado;

    public Vendas() {
    }
    
    public Vendas(String cliente, double valorProduto, String estado) {
        estado = 
        this.cliente = cliente;
        this.valorProduto = valorProduto;
        this.estado = estado;
        
        
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getValorProduto() {
        return valorProduto;
    }

    public void setValorProduto(double valorProduto) {
        this.valorProduto = valorProduto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    
    
    
    
    
}
