/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/**
 *
 * @author LuizGM
 */
public class CalculoICM extends Vendas{

    public double calcRJ(String cliente, double valorProduto, String estado ){
        setCliente(cliente);
        setEstado(estado);
        setValorProduto(valorProduto);
        double resultado = getValorProduto()*RJ;
        return resultado;
    }
    
   
    
    public double calcMA(String cliente, double valorProduto, String estado ){
        setCliente(cliente);
        setEstado(estado);
        setValorProduto(valorProduto);
        
        double resultado = getValorProduto() * MA;
        return resultado;
    
    }

    public double calcSP(String cliente, double valorProduto, String estado) {
      setCliente(cliente);
        setEstado(estado);
        setValorProduto(valorProduto);
        double resultado = getValorProduto()*SP;
        return resultado;   
    }
    
    
}
