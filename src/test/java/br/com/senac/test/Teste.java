/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.CalculoICM;
import br.com.senac.Vendas;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LuizGM
 */
public class Teste {
    
    public Teste() {
    }
    @Test
    public void testCalculoRJ(){
        CalculoICM calculoICM = new CalculoICM();
        double result = calculoICM.calcRJ("Saraiva Rio", 100, "RJ");
        
        assertEquals(17, result, 0.01);
    }
    @Test
    public void testCalcSP(){
        CalculoICM calculoICM = new CalculoICM();
       double resultado = calculoICM.calcSP("Livraria SP", 100, "SP");
       
        assertEquals(18,resultado, 0.01);
       
    }
    @Test
    public void testCalcMA(){
        CalculoICM calculoICM = new CalculoICM();
        double restult = calculoICM.calcMA("Livros Ma", 100, "MA");
        assertEquals(12, restult, 0.01);
    }
}
